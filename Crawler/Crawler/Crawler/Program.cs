﻿using Crawler.data;
using Crawler.lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Crawler
{
    class Program
    {
        /// <summary>
        /// 計時器集合
        /// </summary>
        private static TimerCollection timers = new TimerCollection();

        /// <summary>
        /// HttpClient類別
        /// </summary>
        private static APIRequest apiRequest = new APIRequest();

        /// <summary>
        /// 資料庫暫存
        /// </summary>
        private static DBTemps dbTemps = new DBTemps();

        /// <summary>
        /// 一般計時器更新關閉
        /// </summary>
        private static bool commonTimerClose = false;

        /// <summary>
        /// 主程式
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            // 註冊Nuget包System.Text.Encoding.CodePages中的編碼到.NET Core
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            // 暫存資料初始化
            dbTemps.Init();
            // 設定計時器
            timers.common.Elapsed += CrawlerDemo;
            timers.common.AutoReset = true;
            timers.common.Enabled = true;
            // 設定計時器(即時比分)
            timers.immediate.Elapsed += CrawlerImmediateDemo;
            timers.immediate.AutoReset = true;
            timers.immediate.Enabled = false;

            Console.ReadLine();
        }

        /// <summary>
        /// 爬蟲程式(一般)
        /// </summary>
        private static void CrawlerDemo(Object source, ElapsedEventArgs e)
        {
            try
            {
                Console.WriteLine("timers.common:{0}", DateTime.Now);
                // 一般計時器關閉
                timers.common.Enabled = false;
                // 設定 RequestHeader
                apiRequest.Accept = "*/*";
                apiRequest.AcceptEncoding = "gzip, deflate";
                apiRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36";
                apiRequest.Host = "bf.win007.com";
                apiRequest.Referer = "http://bf.win007.com/NBA_SC_big.aspx?type=sc";
                // XML反序列化物件List
                List<CommonXML.c> xmlList = new List<CommonXML.c>();
                // 儲存昨今明爬蟲字串
                Task<string>[] taskStr = new Task<string>[3];

                // 取得爬蟲資料
                for (int i = 0; i < 3; i++)
                {
                    // 昨今明
                    string day = DateTime.Now.AddDays(i - 1).ToString("yyyy-M-d");
                    apiRequest.Url = $"http://bf.win007.com/nba_date.aspx?date={day}&t={Tool.CreateUnix()}&s=1&h=0&m=0";
                    taskStr[i] = apiRequest.GetAsync();
                }

                // 等待所有Task完成
                bool taskDone = Task.WaitAll(taskStr, 5000);

                // Task在5秒內完成
                if (taskDone)
                {
                    foreach (var item in taskStr)
                    {
                        // 反序列化XML字串
                        CommonXML.c xmlObj = Tool.XmlDeserialize<CommonXML.c>(item.Result);
                        xmlList.Add(xmlObj);
                    }

                    // 爬蟲暫存
                    APITemps apiTemps = new APITemps();
                    // 分析後更改爬蟲暫存
                    Analysis.CommonAnalysis(xmlList, apiTemps);
                    // 暫存表
                    DataTable tempTable = new DataTable();
                    // 聯盟
                    tempTable = Compare.CompareAlliance(dbTemps, apiTemps);

                    // 如果暫存表有資料
                    if (tempTable.Rows.Count > 0)
                    {
                        // 新增聯盟並更新暫存
                        Operate.AddAlliance(dbTemps, tempTable);
                    }

                    // 隊伍
                    tempTable = Compare.CompareTeam(dbTemps, apiTemps);

                    if (tempTable.Rows.Count > 0)
                    {
                        Operate.AddTeam(dbTemps, tempTable);
                    }

                    // 賽事(新增)
                    tempTable = Compare.CompareGame(dbTemps, apiTemps, 0);

                    if (tempTable.Rows.Count > 0)
                    {
                        Operate.AddOrUpOrDelByMode(dbTemps, tempTable, 0);
                    }

                    // 刪除
                    tempTable = Compare.CompareGame(dbTemps, apiTemps, 1);

                    if (tempTable.Rows.Count > 0)
                    {
                        Operate.AddOrUpOrDelByMode(dbTemps, tempTable, 1);
                    }

                    // 即時更新timer啟動時不更新
                    if (!commonTimerClose)
                    {
                        // 修改
                        tempTable = Compare.CompareGame(dbTemps, apiTemps, 2);

                        if (tempTable.Rows.Count > 0)
                        {
                            Operate.AddOrUpOrDelByMode(dbTemps, tempTable, 2);
                        }
                    }

                    // 即時更新timer是否啟動
                    bool chkGameStart = Tool.CheckGameStart(dbTemps.Game);
                    timers.immediate.Enabled = chkGameStart;
                    commonTimerClose = chkGameStart;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message:CrawlerError\n {0}", ex.Message);
            }
            finally
            {
                timers.common.Enabled = true;
                timers.common.Interval = 60 * 1000;
            }
        }

        /// <summary>
        /// 爬蟲程式(即時比分)
        /// </summary>
        private static void CrawlerImmediateDemo(Object source, ElapsedEventArgs e)
        {
            try
            {
                Console.WriteLine("timers.immediate:{0}", DateTime.Now);
                // 即時比分計時器關閉
                timers.immediate.Enabled = false;
                // 設定 RequestHeader
                apiRequest.Accept = "*/*";
                apiRequest.AcceptEncoding = "gzip, deflate";
                apiRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36";
                apiRequest.Host = "lq3.win007.com";
                apiRequest.Referer = "http://lq3.win007.com/";
                apiRequest.Url = "http://lq3.win007.com/NBA/change.xml?Tool.CreateUnix()";

                // 爬蟲回應字串
                Task<string> resStr = apiRequest.GetAsync();
                ImmediateXML.c xmlObj = Tool.XmlDeserialize<ImmediateXML.c>(resStr.Result);

                // 防止object reference not set錯誤
                if (xmlObj.h != null)
                {
                    // 爬蟲暫存
                    APITemps apiTemps = new APITemps();
                    // 分析後更改爬蟲暫存
                    Analysis.ImmediateAnalysis(xmlObj, apiTemps);
                    // 暫存表
                    DataTable tempTable = new DataTable();

                    // 賽事
                    tempTable = Compare.CompareGameImmediate(dbTemps, apiTemps);

                    // 更新
                    if (tempTable.Rows.Count > 0)
                    {
                        Operate.UpGameImmediate(dbTemps, tempTable);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message:CrawlerError\n {0}", ex.Message);
            }
            finally
            {
                // 一般計時更新關閉時在開啟
                timers.immediate.Enabled = commonTimerClose;
            }
        }
    }
}
