﻿#region 功能修改描述與時間 

/*
 * 描述:HttpClient Library
 * 建立日期:2019-07-02
 */

#endregion

using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Crawler.lib
{
    public class APIRequest
    {
        /// <summary>
        /// 能夠接受的回應內容類型
        /// </summary>
        public string Accept { get; set; }

        /// <summary>
        /// 能夠接受的編碼方式列表
        /// </summary>
        public string AcceptEncoding { get; set; }

        /// <summary>
        /// 瀏覽器的瀏覽器身分標識字串
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 伺服器的域名
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 瀏覽器所存取的前一個頁面
        /// </summary>
        public string Referer { get; set; }

        /// <summary>
        /// 網址
        /// </summary>
        public string Url { get; set; }


        /// <summary>
        /// HttpClient 使用的預設訊息處理常式。
        /// </summary>
        private static HttpClientHandler handler = new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate };

        /// <summary>
        /// HttpClient Get
        /// </summary>
        /// <param name="getInfo">/param>
        /// <returns></returns>
        public Task<string> GetAsync()
        {
            // 建立HttpClient
            HttpClient client = new HttpClient(handler);

            try
            {
                // 設定RequestHeader
                client.DefaultRequestHeaders.Add("Accept", Accept);
                client.DefaultRequestHeaders.Add("Accept-Encoding", AcceptEncoding);
                client.DefaultRequestHeaders.Add("User-Agent", UserAgent);
                client.DefaultRequestHeaders.Add("Host", Host);
                client.DefaultRequestHeaders.Add("Referer",Referer);
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine("API Error\nMessage: {0}", ex.Message);
            }
            return client.GetStringAsync(Url);
        }
    }
}
