﻿#region 功能修改描述與時間 

/*
 * 描述:資料表操作
 * 建立日期:2019-07-12
 */

#endregion

using Crawler.data;
using System.Data;
using System.Data.SqlClient;

namespace Crawler.lib
{
    public class Operate
    {
        /// <summary>
        /// 新增聯賽
        /// </summary>
        public static void AddAlliance(DBTemps dbTemps, DataTable tempTable)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pro_addAlliance";
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@inputTable",
                SqlDbType = SqlDbType.Structured,

                Value = tempTable,
                Direction = ParameterDirection.Input
            });
            DataTable dt = ConnectionDB.GetDataTable(cmd);
            if (dt.Rows.Count > 0) dbTemps.Alliance = dt;
        }

        /// <summary>
        /// 新增隊伍
        /// </summary>
        public static void AddTeam(DBTemps dbTemps, DataTable tempTable)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pro_addTeam";
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@inputTable",
                SqlDbType = SqlDbType.Structured,

                Value = tempTable,
                Direction = ParameterDirection.Input
            });
            DataTable dt = ConnectionDB.GetDataTable(cmd);
            if (dt.Rows.Count > 0) dbTemps.Team = dt;
        }

        /// <summary>
        /// 新增 OR 更新 OR 刪除賽事
        /// </summary>
        /// <param name="mode"></param>
        public static void AddOrUpOrDelByMode(DBTemps dbTemps, DataTable tempTable, int mode)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pro_addOrUpOrDelGame";
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@inputTable",
                SqlDbType = SqlDbType.Structured,

                Value = tempTable,
                Direction = ParameterDirection.Input
            });
            cmd.Parameters.Add(new SqlParameter("@mode", mode));
            DataTable dt = ConnectionDB.GetDataTable(cmd);
            if (dt.Rows.Count > 0) dbTemps.Game = dt;
        }

        /// <summary>
        /// 即時更新賽事
        /// </summary>
        public static void UpGameImmediate(DBTemps dbTemps, DataTable tempTable)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pro_upGameImmediate";
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@inputTable",
                SqlDbType = SqlDbType.Structured,

                Value = tempTable,
                Direction = ParameterDirection.Input
            });
            DataTable dt = ConnectionDB.GetDataTable(cmd);
            if (dt.Rows.Count > 0) dbTemps.Game = dt;
        }
    }
}
