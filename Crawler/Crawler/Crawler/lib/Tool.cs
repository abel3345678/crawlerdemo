﻿#region MyRegion
/*
 * 描述:工具類別
 * 建立日期:2019-07-03
 */
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Crawler.lib
{
    public class Tool
    {
        /// <summary>
        /// 移除欄位相同資料
        /// </summary>
        /// <param name="Field">根據欄位</param>
        /// <returns></returns>
        public static void DelSameField(DataTable dt, string Field)
        {
            // 去除重覆值(聯賽)
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = dt.Rows.Count - 1; j > i; j--)
                {
                    if (dt.Rows[i][Field].Equals(dt.Rows[j][Field]))
                    {
                        dt.Rows.RemoveAt(j);
                    }
                }
            }
        }

        /// <summary>
        /// 印出datatable
        /// </summary>
        /// <param name="dt"></param>
        public static void ConsoleTable(DataTable dt)
        {
            foreach (DataColumn col in dt.Columns)
            {
                Console.Write("{0} ", col.ColumnName);
            }
            Console.WriteLine();
            Console.WriteLine("---------------------");

            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    Console.Write("{0} ", row[col]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("--------------------");
            Console.WriteLine();
        }

        /// <summary>
        /// 產生時間戳記
        /// </summary>
        /// <returns></returns>
        public static string CreateUnix()
        {
            // 當地時區
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            // 相差秒數
            long timeStamp = (long)(DateTime.Now - startTime).TotalSeconds;

            return $"{ timeStamp}000";
        }

        /// <summary>
        /// 轉換空字串變0
        /// </summary>
        /// <param name="inStr">輸入字串</param>
        /// <returns></returns>
        public static string EmptyStrToZero(string inStr)
        {
            return string.IsNullOrEmpty(inStr) ? "0" : inStr;
        }

        /// <summary>
        /// 確認今日開賽
        /// </summary>
        /// <param name="dt">賽事暫存</param>
        /// <param name="today">今天</param>
        /// <returns></returns>
        public static bool CheckGameStart(DataTable dt)
        {
            bool result = false;
            // 取得日期
            string today = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime sdate = Convert.ToDateTime(today);
            DateTime edate = Convert.ToDateTime(today).AddHours(23).AddMinutes(59).AddSeconds(59);

            // 確認目前是否有開賽
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime startTime = Convert.ToDateTime(dt.Rows[i]["f_startTime"]);
                int status = Convert.ToInt16(dt.Rows[i]["f_status"]);
                if (startTime >= sdate && startTime < edate && status > 0)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// 將物件序列化成XML格式字串
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="obj">物件</param>
        /// <returns>XML格式字串</returns>
        public static string XmlSerialize<T>(T obj) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(writer, obj);
                return stringWriter.ToString();
            }
        }

        /// <summary>
        /// 將XML格式字串反序列化成物件
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="xmlString">XML格式字串</param>
        /// <returns>反序列化後的物件</returns>
        public static T XmlDeserialize<T>(string xmlString) where T : class
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                object deserializationObj = deserializer.Deserialize(reader);
                return deserializationObj as T;
            };
        }

        /// <summary>
        /// 創建DataTale藉由名子
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DataTable CrateTableByName(string name)
        {
            DataTable dt = new DataTable();
            switch (name)
            {
                // 聯賽 & 隊伍
                case "Alliance":
                case "Team":
                    dt.Columns.Add("f_webName");
                    dt.Columns.Add("f_nameTw");
                    break;
                // 賽事
                case "Game":
                    dt.Columns.Add("f_webId");
                    dt.Columns.Add("f_webName");
                    dt.Columns.Add("f_teamAName");
                    dt.Columns.Add("f_teamBName");
                    dt.Columns.Add("f_teamAScore");
                    dt.Columns.Add("f_teamBScore");
                    dt.Columns.Add("f_teamATotalScore");
                    dt.Columns.Add("f_teamBTotalScore");
                    dt.Columns.Add("f_startTime");
                    dt.Columns.Add("f_actualStartTime");
                    dt.Columns.Add("f_status");
                    break;
                // 賽事(即時更新)
                case "GameImmediate":
                    dt.Columns.Add("f_webId");
                    dt.Columns.Add("f_teamAScore");
                    dt.Columns.Add("f_teamBScore");
                    dt.Columns.Add("f_teamATotalScore");
                    dt.Columns.Add("f_teamBTotalScore");
                    dt.Columns.Add("f_status");
                    break;
            }
            return dt;
        }

        /// <summary>
        /// 將集合轉換為DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="varlist"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names 
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others will follow 
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }
    }
}
