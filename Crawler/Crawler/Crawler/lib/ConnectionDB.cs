﻿#region 

/*
 * 描述:DB操作
 * 建立日期:2019-07-04
*/

#endregion

using System;
using System.Data;
using System.Data.SqlClient;

namespace Crawler.lib
{
    public class ConnectionDB
    {
        // 連線字串
        private static string constr = "Data Source=localhost;Initial Catalog=t_crawlerDemo;Persist Security Info=True;User ID=sa;Password=aa88889999;MultipleActiveResultSets=True";

        /// <summary>
        /// 傳回一個資料表的處理，速度比DataReader慢，但因為是讀回來存放在網站記憶體，所以使用上可以較靈活
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataTable GetDataTable(SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(); 
            DataTable dt = new DataTable();

            try
            {
                cmd.Connection = new SqlConnection(constr); 
                cmd.Connection.Open(); 
                da.SelectCommand = cmd; 
                da.Fill(dt); 
                cmd.Connection.Close(); 
            }
            catch (Exception ex) 
            {
                Console.WriteLine("SQL Error\nMessage: {0}", ex);
                dt = new DataTable();
            }
            finally
            {
                cmd.Parameters.Clear();
                if (cmd.Connection.State != ConnectionState.Closed)
                    cmd.Connection.Close();
            }

            return dt;
        }
        /// <summary>
        /// 通常用於傳回多個資料表的處理，其實就是DataTable的集合
        /// </summary>
        /// <param name="cmdLst"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(); 
            DataSet ds = new DataSet(); 

            try
            {
                cmd.Connection = new SqlConnection(constr);
                cmd.Connection.Open();
                da.SelectCommand = cmd;
                da.Fill(ds);
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("SQL Error\nMessage: {0}", ex.Message);
                ds = new DataSet();
            }

            return ds;
        }

        /// <summary>
        /// 執行新刪修
        /// </summary>
        /// <param name="meta">定義對應UDP欄位</param>
        /// <param name="cmd">執行方法</param>
        /// <returns></returns>
        public static int ExecuteQuery(SqlCommand cmd)
        {
            int result = 0;

            try
            {
                cmd.Connection = new SqlConnection(constr);
                cmd.Connection.Open();
                result = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("SQL Error\nMessage: {0}", ex.Message);
                result = 0;
            }
            finally
            {
                cmd.Parameters.Clear();
                if (cmd.Connection.State != ConnectionState.Closed)
                    cmd.Connection.Close();
            }
            return result;
        }
    }
}
