﻿#region

/*
 * 描述: 各類表比對操作
 * 建立日期:2019-07-12
*/

#endregion


using Crawler.data;
using System;
using System.Data;
using System.Linq;

namespace Crawler.lib
{
    public class Compare
    {
        /// <summary>
        /// 比較(聯盟)
        /// </summary>
        public static DataTable CompareAlliance(DBTemps dbTemps, APITemps apiTemps)
        {
            DataTable tempTable = new DataTable();
            var insert = from t_allianceAPI in apiTemps.Alliance.AsEnumerable()
                         join t_allianceDB in dbTemps.Alliance.AsEnumerable() on t_allianceAPI.Field<string>("f_webName") equals t_allianceDB.Field<string>("f_webName")
                         into g
                         from t_allianceDB in g.DefaultIfEmpty()
                         where t_allianceDB == null
                         select new
                         {
                             f_webName = t_allianceAPI.Field<string>("f_webName"),
                             f_nameTw = t_allianceAPI.Field<string>("f_nameTw"),
                         };
            tempTable = Tool.ConvertToDataTable(insert);
            return tempTable;
        }

        /// <summary>
        /// 比較(隊伍)
        /// </summary>
        public static DataTable CompareTeam(DBTemps dbTemps, APITemps apiTemps)
        {
            DataTable tempTable = new DataTable();
            var insert = from t_teamAPI in apiTemps.Team.AsEnumerable()
                         join t_teamDB in dbTemps.Team.AsEnumerable() on t_teamAPI.Field<string>("f_webName") equals t_teamDB.Field<string>("f_webName")
                         into g
                         from t_teamDB in g.DefaultIfEmpty()
                         where t_teamDB == null
                         select new
                         {
                             f_webName = t_teamAPI.Field<string>("f_webName"),
                             f_nameTw = t_teamAPI.Field<string>("f_nameTw"),
                         };
            tempTable = Tool.ConvertToDataTable(insert);
            return tempTable;
        }

        /// <summary>
        /// 比較(賽事)
        /// </summary>
        /// <param name="mode">模式{0:新增,1刪除,2:修改}</param>
        public static DataTable CompareGame(DBTemps dbTemps, APITemps apiTemps,int mode)
        {
            DataTable tempTable = new DataTable();
            switch (mode)
            {
                case 0:
                    #region 新增

                    // 以爬蟲資料為主表的left join找出要insert的值
                    var add = from t_gameAPI in apiTemps.Game.AsEnumerable()
                              join t_allianceDB in dbTemps.Alliance.AsEnumerable() on t_gameAPI.Field<string>("f_webName") equals t_allianceDB.Field<string>("f_webName")
                              join t_teamADB in dbTemps.Team.AsEnumerable() on t_gameAPI.Field<string>("f_teamAName") equals t_teamADB.Field<string>("f_nameTw")
                              join t_teamBDB in dbTemps.Team.AsEnumerable() on t_gameAPI.Field<string>("f_teamBName") equals t_teamBDB.Field<string>("f_nameTw")
                              join t_gameDB in dbTemps.Game.AsEnumerable() on t_gameAPI.Field<string>("f_webId") equals t_gameDB.Field<string>("f_webId")
                              into g
                              from t_gameDB in g.DefaultIfEmpty()
                              where t_gameDB == null
                              select new
                              {
                                  f_webId = t_gameAPI.Field<string>("f_webId"),
                                  f_comId = t_allianceDB.Field<int>("f_id"),
                                  f_teamAId = t_teamADB.Field<int>("f_id"),
                                  f_teamBId = t_teamBDB.Field<int>("f_id"),
                                  f_teamAScore = t_gameAPI.Field<string>("f_teamAScore"),
                                  f_teamBScore = t_gameAPI.Field<string>("f_teamBScore"),
                                  f_teamATotalScore = t_gameAPI.Field<string>("f_teamATotalScore"),
                                  f_teamBTotalScore = t_gameAPI.Field<string>("f_teamBTotalScore"),
                                  f_startTime = DateTime.Parse(t_gameAPI.Field<string>("f_startTime")),
                                  f_actualStartTime = (DateTime?)null,
                                  f_status = int.Parse(t_gameAPI.Field<string>("f_status"))
                              };
                    tempTable = Tool.ConvertToDataTable(add);

                    #endregion
                    break;
                case 1:
                    #region 刪除

                    // 取得日期
                    string today = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime sdate = Convert.ToDateTime(today);
                    DateTime edate = Convert.ToDateTime(today).AddHours(23).AddMinutes(59).AddSeconds(59);
                    // 以DB資料為主表的left join配合時間區間找出要delete的值
                    var del = from t_gameDB in dbTemps.Game.AsEnumerable()
                              join t_gameAPI in apiTemps.Game.AsEnumerable() on t_gameDB.Field<string>("f_webId") equals t_gameAPI.Field<string>("f_webId")
                              into g
                              from t_gameAPI in g.DefaultIfEmpty()
                              where t_gameAPI == null && (t_gameDB.Field<DateTime>("f_startTime") >= sdate && t_gameDB.Field<DateTime>("f_startTime") <= edate)
                              select new
                              {
                                  f_webId = t_gameDB.Field<string>("f_webId"),
                                  f_comId = t_gameDB.Field<int>("f_comId"),
                                  f_teamAId = t_gameDB.Field<int>("f_teamAId"),
                                  f_teamBId = t_gameDB.Field<int>("f_teamBId"),
                                  f_teamAScore = t_gameDB.Field<string>("f_teamAScore"),
                                  f_teamBScore = t_gameDB.Field<string>("f_teamBScore"),
                                  f_teamATotalScore = t_gameDB.Field<string>("f_teamATotalScore"),
                                  f_teamBTotalScore = t_gameDB.Field<string>("f_teamBTotalScore"),
                                  f_startTime = t_gameDB.Field<DateTime>("f_startTime"),
                                  f_actualStartTime = (DateTime?)null,
                                  f_status = t_gameDB.Field<Int16>("f_status")
                              };
                    tempTable = Tool.ConvertToDataTable(del);

                    #endregion
                    break;
                case 2:
                    #region 更新

                    // 狀態有變更再更新
                    var up = from t_gameAPI in apiTemps.Game.AsEnumerable()
                             join t_allianceDB in dbTemps.Alliance.AsEnumerable() on t_gameAPI.Field<string>("f_webName") equals t_allianceDB.Field<string>("f_webName")
                             join t_teamADB in dbTemps.Team.AsEnumerable() on t_gameAPI.Field<string>("f_teamAName") equals t_teamADB.Field<string>("f_nameTw")
                             join t_teamBDB in dbTemps.Team.AsEnumerable() on t_gameAPI.Field<string>("f_teamBName") equals t_teamBDB.Field<string>("f_nameTw")
                             join t_gameDB in dbTemps.Game.AsEnumerable() on t_gameAPI.Field<string>("f_webId") equals t_gameDB.Field<string>("f_webId")
                             where t_gameDB.Field<int>("f_comId") != t_allianceDB.Field<int>("f_id") ||
                                   t_gameDB.Field<int>("f_teamAId") != t_teamADB.Field<int>("f_id") ||
                                   t_gameDB.Field<int>("f_teamBId") != t_teamBDB.Field<int>("f_id") ||
                                   t_gameDB.Field<string>("f_teamAScore") != t_gameAPI.Field<string>("f_teamAScore") ||
                                   t_gameDB.Field<string>("f_teamBScore") != t_gameAPI.Field<string>("f_teamBScore") ||
                                   t_gameDB.Field<string>("f_teamATotalScore") != t_gameAPI.Field<string>("f_teamATotalScore") ||
                                   t_gameDB.Field<string>("f_teamBTotalScore") != t_gameAPI.Field<string>("f_teamBTotalScore") ||
                                   t_gameDB.Field<DateTime>("f_startTime") != DateTime.Parse(t_gameAPI.Field<string>("f_startTime")) ||
                                   t_gameDB.Field<Int16>("f_status") != int.Parse(t_gameAPI.Field<string>("f_status"))
                             select new
                             {
                                 f_webId = t_gameAPI.Field<string>("f_webId"),
                                 f_comId = t_allianceDB.Field<int>("f_id"),
                                 f_teamAId = t_teamADB.Field<int>("f_id"),
                                 f_teamBId = t_teamBDB.Field<int>("f_id"),
                                 f_teamAScore = t_gameAPI.Field<string>("f_teamAScore"),
                                 f_teamBScore = t_gameAPI.Field<string>("f_teamBScore"),
                                 f_teamATotalScore = t_gameAPI.Field<string>("f_teamATotalScore"),
                                 f_teamBTotalScore = t_gameAPI.Field<string>("f_teamBTotalScore"),
                                 f_startTime = DateTime.Parse(t_gameAPI.Field<string>("f_startTime")),
                                 f_actualStartTime = (t_gameAPI.Field<string>("f_status") == "1" && t_gameDB.Field<Int16>("f_status") == 0) ? DateTime.Now : (DateTime?)null,
                                 f_status = int.Parse(t_gameAPI.Field<string>("f_status"))
                             };
                    tempTable = Tool.ConvertToDataTable(up);

                    #endregion
                    break;
            }
            Console.WriteLine($"Mode is: {mode}\n");
            Tool.ConsoleTable(tempTable);
            return tempTable;
        }

        /// <summary>
        /// 比較(賽事-即時更新)
        /// </summary>
        public static DataTable CompareGameImmediate(DBTemps dbTemps, APITemps apiTemps)
        {
            DataTable tempTable = new DataTable();
            // 狀態有變更再更新
            var up = from t_gameAPI in apiTemps.Game.AsEnumerable()
                     join t_gameDB in dbTemps.Game.AsEnumerable() on t_gameAPI.Field<string>("f_webId") equals t_gameDB.Field<string>("f_webId")
                     where t_gameDB.Field<string>("f_teamAScore") != t_gameAPI.Field<string>("f_teamAScore") ||
                           t_gameDB.Field<string>("f_teamBScore") != t_gameAPI.Field<string>("f_teamBScore") ||
                           t_gameDB.Field<string>("f_teamATotalScore") != t_gameAPI.Field<string>("f_teamATotalScore") ||
                           t_gameDB.Field<string>("f_teamBTotalScore") != t_gameAPI.Field<string>("f_teamBTotalScore") ||
                           t_gameDB.Field<Int16>("f_status") != int.Parse(t_gameAPI.Field<string>("f_status"))
                     select new
                     {
                         f_webId = t_gameAPI.Field<string>("f_webId"),
                         f_teamAScore = t_gameAPI.Field<string>("f_teamAScore"),
                         f_teamBScore = t_gameAPI.Field<string>("f_teamBScore"),
                         f_teamATotalScore = t_gameAPI.Field<string>("f_teamATotalScore"),
                         f_teamBTotalScore = t_gameAPI.Field<string>("f_teamBTotalScore"),
                         f_status = int.Parse(t_gameAPI.Field<string>("f_status"))
                     };
            tempTable = Tool.ConvertToDataTable(up);

            Console.WriteLine($"Mode is: 2\n");
            Tool.ConsoleTable(tempTable);

            return tempTable;
        }
    }
}
