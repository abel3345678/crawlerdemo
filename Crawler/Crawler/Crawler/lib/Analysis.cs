﻿#region

/*
 * 描述:解析爬蟲資料
 * 建立日期:2019-07-08
*/

#endregion

using Crawler.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace Crawler.lib
{
    public class Analysis
    {
        /// <summary>
        /// 球探體育一般分析
        /// </summary>
        /// <param name="xmlList">爬蟲資料</param>
        /// <param name="apiTemps">爬蟲暫存</param>
        /// <returns></returns>
        public static void CommonAnalysis(List<CommonXML.c> xmlList, APITemps apiTemps)
        {
            // 聯賽
            apiTemps.Alliance = Tool.CrateTableByName("Alliance");
            // 隊伍
            apiTemps.Team = Tool.CrateTableByName("Team");
            // 賽事
            apiTemps.Game = Tool.CrateTableByName("Game");

            foreach (var xmlObj in xmlList)
            {
                // 解析資料
                for (int i = 0; i < xmlObj.m.Length; i++)
                {
                    // 暫存陣列
                    string[] tmpArr = xmlObj.m[i].Split("^");
                    // 賽事ID
                    string webId = tmpArr[0];
                    // 來源名稱
                    string webName = tmpArr[1].Split(",")[1];
                    // 主隊得分
                    string teamASocre = $"{Tool.EmptyStrToZero(tmpArr[13])},{Tool.EmptyStrToZero(tmpArr[15])},{Tool.EmptyStrToZero(tmpArr[17])},{Tool.EmptyStrToZero(tmpArr[19])},{Tool.EmptyStrToZero(tmpArr[22])},{Tool.EmptyStrToZero(tmpArr[24])},{Tool.EmptyStrToZero(tmpArr[26])}";
                    // 客隊得分
                    string teamBSocre = $"{Tool.EmptyStrToZero(tmpArr[14])},{Tool.EmptyStrToZero(tmpArr[16])},{Tool.EmptyStrToZero(tmpArr[18])},{Tool.EmptyStrToZero(tmpArr[20])},{Tool.EmptyStrToZero(tmpArr[23])},{Tool.EmptyStrToZero(tmpArr[25])},{Tool.EmptyStrToZero(tmpArr[27])}";
                    // 主隊總分
                    string teamATotalScore = tmpArr[11];
                    // 客隊總分
                    string teamBTotalScore = tmpArr[12];
                    // 主隊名
                    string teamAName = Regex.Replace(tmpArr[8].Split(",")[1], @"\[.*\]", "");
                    // 客隊名
                    string teamBName = Regex.Replace(tmpArr[10].Split(",")[1], @"\[.*\]", "");
                    // 開賽時間
                    string startTime = $"{tmpArr[42]}-{tmpArr[4].Replace("月", "-").Replace("日", "").Replace("<br>", " ")}";
                    // 狀態
                    int status = Convert.ToInt16(tmpArr[5]);

                    // 聯盟
                    DataRow rowAlliance;
                    rowAlliance = apiTemps.Alliance.NewRow();
                    rowAlliance["f_webName"] = webName;
                    rowAlliance["f_nameTw"] = webName;
                    // 主隊
                    DataRow rowTeamA;
                    rowTeamA = apiTemps.Team.NewRow();
                    rowTeamA["f_webName"] = webName;
                    rowTeamA["f_nameTw"] = teamAName;
                    // 客隊
                    DataRow rowTeamB;
                    rowTeamB = apiTemps.Team.NewRow();
                    rowTeamB["f_webName"] = webName;
                    rowTeamB["f_nameTw"] = teamBName;
                    // 賽事
                    DataRow rowGame;
                    rowGame = apiTemps.Game.NewRow();
                    rowGame["f_webId"] = webId;
                    rowGame["f_webName"] = webName;
                    rowGame["f_teamAName"] = teamAName;
                    rowGame["f_teamBName"] = teamBName;
                    rowGame["f_teamAScore"] = teamASocre;
                    rowGame["f_teamBScore"] = teamBSocre;
                    rowGame["f_teamATotalScore"] = teamATotalScore;
                    rowGame["f_teamBTotalScore"] = teamBTotalScore;
                    rowGame["f_startTime"] = startTime;
                    rowGame["f_status"] = status;
                    // 新增列
                    apiTemps.Alliance.Rows.Add(rowAlliance);
                    apiTemps.Team.Rows.Add(rowTeamA);
                    apiTemps.Team.Rows.Add(rowTeamB);
                    apiTemps.Game.Rows.Add(rowGame);
                }
            }

            // 去除重複資料
            Tool.DelSameField(apiTemps.Alliance, "f_webName");
            Tool.DelSameField(apiTemps.Team, "f_nameTw");
            Tool.DelSameField(apiTemps.Game, "f_webId");
        }

        /// <summary>
        /// 球探體育即時更新分析(賽事)
        /// </summary>
        /// <param name="xmlObj"></param>
        /// <returns></returns>
        public static void ImmediateAnalysis(ImmediateXML.c xmlObj, APITemps apiTemps)
        {
            // 賽事
            // 賽事
            apiTemps.Game = Tool.CrateTableByName("GameImmediate");

            for (int i = 0; i < xmlObj.h.Length; i++)
            {
                // 暫存陣列
                string[] tmpArr = xmlObj.h[i].Split("^");
                // 賽事ID
                string webId = tmpArr[0];
                // 主隊得分
                string teamASocre = $"{Tool.EmptyStrToZero(tmpArr[5])},{Tool.EmptyStrToZero(tmpArr[7])},{Tool.EmptyStrToZero(tmpArr[9])},{Tool.EmptyStrToZero(tmpArr[11])},{Tool.EmptyStrToZero(tmpArr[16])},{Tool.EmptyStrToZero(tmpArr[18])},{Tool.EmptyStrToZero(tmpArr[20])}";
                // 客隊得分
                string teamBSocre = $"{Tool.EmptyStrToZero(tmpArr[6])},{Tool.EmptyStrToZero(tmpArr[8])},{Tool.EmptyStrToZero(tmpArr[10])},{Tool.EmptyStrToZero(tmpArr[12])},{Tool.EmptyStrToZero(tmpArr[17])},{Tool.EmptyStrToZero(tmpArr[19])},{Tool.EmptyStrToZero(tmpArr[21])}";
                // 主隊總分
                string teamATotalScore = tmpArr[3];
                // 客隊總分
                string teamBTotalScore = tmpArr[4];
                // 狀態
                int status = Convert.ToInt16(tmpArr[1]);

                // 賽事
                DataRow rowGame;
                rowGame = apiTemps.Game.NewRow();
                rowGame["f_webId"] = webId;
                rowGame["f_teamAScore"] = teamASocre;
                rowGame["f_teamBScore"] = teamBSocre;
                rowGame["f_teamATotalScore"] = teamATotalScore;
                rowGame["f_teamBTotalScore"] = teamBTotalScore;
                rowGame["f_status"] = status;
                apiTemps.Game.Rows.Add(rowGame);
            }
        }
    }
}
