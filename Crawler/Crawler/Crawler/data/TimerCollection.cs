﻿#region 功能修改描述與時間 

/*
 * 描述: 計時器集合
 * 建立日期:2019-07-15
*/

#endregion

using System.Timers;

namespace Crawler.data
{
    /// <summary>
    /// 計時器
    /// </summary>
    public class TimerCollection
    {
        /// <summary>
        /// 一般計時器
        /// </summary>
        public Timer common = new Timer();

        /// <summary>
        /// 計時器(即時比分)
        /// </summary>
        public Timer immediate = new Timer(3000);
    }
}
