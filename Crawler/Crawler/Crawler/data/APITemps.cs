﻿#region 功能修改描述與時間 

/*
 * 描述:爬蟲資料暫存
 * 建立日期:2019-07-15
*/

#endregion

using System.Data;

namespace Crawler.data
{
    /// <summary>
    /// 爬蟲暫存
    /// </summary>
    public class APITemps
    {
        /// <summary>
        /// 爬蟲聯盟暫存
        /// </summary>
        public DataTable Alliance { get; set; }

        /// <summary>
        /// 爬蟲隊伍暫存
        /// </summary>
        public DataTable Team { get; set; }

        /// <summary>
        /// 爬蟲賽事暫存
        /// </summary>
        public DataTable Game { get; set; }
    }
}
