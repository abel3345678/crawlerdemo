﻿#region 功能修改描述與時間 

/*
 * 描述:XML反序列化物件
 * 建立日期:2019-07-15
*/

#endregion

namespace Crawler.data
{
    /// <summary>
    /// 一般更新XML物件
    /// </summary>
    public class CommonXML
    {
        // 注意: 生成的代码可能至少需要 .NET Framework 4.5 或 .NET Core/Standard 2.0。
        // 抓回資料(反序列化使用)
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class c
        {
            private string[] mField;

            private string sField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("h", IsNullable = false)]
            public string[] m
            {
                get
                {
                    return this.mField;
                }
                set
                {
                    this.mField = value;
                }
            }

            /// <remarks/>
            public string s
            {
                get
                {
                    return this.sField;
                }
                set
                {
                    this.sField = value;
                }
            }
        }
    }

    /// <summary>
    /// 即時比分XML物件
    /// </summary>
    public class ImmediateXML
    {
        // 注意: 生成的代码可能至少需要 .NET Framework 4.5 或 .NET Core/Standard 2.0。
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class c
        {
            private string[] hField;

            private byte refreshField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("h")]
            public string[] h
            {
                get
                {
                    return this.hField;
                }
                set
                {
                    this.hField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte refresh
            {
                get
                {
                    return this.refreshField;
                }
                set
                {
                    this.refreshField = value;
                }
            }
        }


    }
}
