﻿#region 功能修改描述與時間 

/*
 * 描述:資料庫資料暫存
 * 建立日期:2019-07-15
*/

#endregion

using Crawler.lib;
using System.Data;
using System.Data.SqlClient;

namespace Crawler.data
{
    /// <summary>
    /// DB暫存
    /// </summary>
    public class DBTemps
    {
        /// <summary>
        /// 資料庫暫存集合
        /// </summary>
        private DataSet ds = ConnectionDB.GetDataSet(new SqlCommand(@"EXEC pro_selInit"));

        /// <summary>
        /// 資料庫聯盟暫存
        /// </summary>
        public DataTable Alliance { get; set; }
        /// <summary>
        /// 資料庫隊伍暫存
        /// </summary>
        public DataTable Team { get; set; }

        /// <summary>
        /// 資料庫賽事暫存
        /// </summary>
        public DataTable Game { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            Alliance = ds.Tables[0];
            Team = ds.Tables[1];
            Game = ds.Tables[2];
        }
    }
}
