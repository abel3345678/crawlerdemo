/*
    描述: 用戶定義類型
    建立時間:  2019-07-10
*/

CREATE TYPE [dbo].[type_gameImmediate] AS TABLE(
    [f_webId] [nvarchar](50) NULL,
    [f_teamAScore] [nvarchar](50) NULL,
    [f_teamBScore] [nvarchar](50) NULL,
    [f_teamATotalScore] [nvarchar](50) NULL,
    [f_teamBTotalScore] [nvarchar](50) NULL,
    [f_status] [smallint] NULL
)
