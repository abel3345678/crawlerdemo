/*
    描述: 用戶定義類型
    建立時間:  2019-07-09
*/

CREATE TYPE [dbo].[type_team] AS TABLE(
    [f_webName] [nvarchar](50) NULL,
    [f_nameTw] [nvarchar](50) NULL
)