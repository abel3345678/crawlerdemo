/*
	描述: 新建隊伍、更新、刪除賽事
	建立日期: 2019-07-10
*/

CREATE PROCEDURE [dbo].[pro_addOrUpOrDelGame]
    @mode INT,--模式 0:新增 1:刪除 2:修改
    @inputTable [dbo].[type_game] READONLY
AS
BEGIN
    -- 新增
    IF(@mode = 0)
    BEGIN
        INSERT INTO [t_crawlerDemo].[dbo].[t_game](f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_status)
        SELECT f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_status FROM @inputTable
        
        SELECT f_id, f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_actualStartTime, f_status
	    FROM [t_crawlerDemo].[dbo].[t_game] WITH(NOLOCK)
    END
    -- 刪除
    ELSE IF(@mode = 1)
    BEGIN
        DELETE A FROM [t_crawlerDemo].[dbo].[t_game] AS A INNER JOIN @inputTable AS B ON A.f_webId = B.f_webId

        SELECT f_id, f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_actualStartTime, f_status
	    FROM [t_crawlerDemo].[dbo].[t_game] WITH(NOLOCK) 
    END
    -- 更新
    ELSE IF(@mode = 2)
    BEGIN
        UPDATE [t_crawlerDemo].[dbo].[t_game] 
        SET f_comId = B.f_comId,
            f_teamAId = B.f_teamAId,
            f_teamBId = B.f_teamBId,
            f_teamAScore = B.f_teamAScore,
            f_teamBScore = B.f_teamBScore,
            f_teamATotalScore = B.f_teamATotalScore,
            f_teamBTotalScore = B.f_teamBTotalScore,
            f_startTime = B.f_startTime,
            f_actualStartTime = B.f_actualStartTime,
            f_status = B.f_status,
            f_updateDate = GETDATE()
        FROM [t_crawlerDemo].[dbo].[t_game] AS A
        INNER JOIN @inputTable AS B ON A.f_webId = B.f_webId

        SELECT f_id, f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_actualStartTime, f_status
	    FROM [t_crawlerDemo].[dbo].[t_game] WITH(NOLOCK)  
    END
END