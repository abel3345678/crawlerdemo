/*
	描述: 新建隊伍
	建立日期: 2019-07-09
*/

CREATE PROCEDURE [dbo].[pro_addTeam]
    @inputTable [dbo].[type_team] READONLY
AS
BEGIN
    INSERT INTO [t_crawlerDemo].[dbo].[t_team](f_webName, f_nameTw)
    SELECT f_webName, f_nameTw FROM @inputTable

    SELECT f_id, f_webName, f_nameTw
	FROM [t_crawlerDemo].[dbo].[t_team] WITH(NOLOCK)
END