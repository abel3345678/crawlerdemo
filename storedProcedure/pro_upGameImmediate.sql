/*
	描述: 即時更新賽事
	建立日期: 2019-07-11
*/

CREATE PROCEDURE [dbo].[pro_upGameImmediate]
    @inputTable [dbo].[type_gameImmediate] READONLY
AS
BEGIN
    -- 即時更新
    BEGIN
        UPDATE [t_crawlerDemo].[dbo].[t_game] 
        SET f_teamAScore = B.f_teamAScore,
            f_teamBScore = B.f_teamBScore,
            f_teamATotalScore = B.f_teamATotalScore,
            f_teamBTotalScore = B.f_teamBTotalScore,
            f_status = B.f_status,
            f_updateDate = GETDATE()
        FROM [t_crawlerDemo].[dbo].[t_game] AS A
        INNER JOIN @inputTable AS B ON A.f_webId = B.f_webId

        SELECT f_id, f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_actualStartTime, f_status
	    FROM [t_crawlerDemo].[dbo].[t_game] WITH(NOLOCK)   
    END
END