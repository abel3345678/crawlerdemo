/*
	描述: 新建聯賽
	建立日期: 2019-07-09
*/

CREATE PROCEDURE [dbo].[pro_addAlliance]
    @inputTable [dbo].[type_alliance] READONLY
AS
BEGIN
    INSERT INTO [t_crawlerDemo].[dbo].[t_alliance](f_webName, f_nameTw)
    SELECT f_webName, f_nameTw FROM @inputTable

	SELECT f_id, f_webName, f_nameTw
	FROM [t_crawlerDemo].[dbo].[t_alliance] WITH(NOLOCK)
END