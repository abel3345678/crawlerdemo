/*
	描述:暂存资料
	建立日期: 2019-07-09
*/

CREATE PROCEDURE [dbo].[pro_selInit]
AS
BEGIN
    SET NOCOUNT ON;
	SELECT f_id, f_webName, f_nameTw
	FROM [t_crawlerDemo].[dbo].[t_alliance] WITH(NOLOCK)
 
    SELECT f_id, f_webName, f_nameTw
	FROM [t_crawlerDemo].[dbo].[t_team] WITH(NOLOCK)
 
    SELECT f_id, f_webId, f_comId, f_teamAId, f_teamBId, f_teamAScore, f_teamBScore, f_teamATotalScore, f_teamBTotalScore, f_startTime, f_actualStartTime, f_status
	FROM [t_crawlerDemo].[dbo].[t_game] WITH(NOLOCK)
END